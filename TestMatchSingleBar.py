import unittest
import mdup


class TestMatchSingleBar(unittest.TestCase):
    def test_3_4_basic(self):
        first_track_name = './test/1/mdup-test-8.mid'
        second_track_name = './test/1/mdup-test-16.mid'
        track_1 = mdup.parse_tracks(first_track_name)[0]
        track_2 = mdup.parse_tracks(second_track_name)[0]
        self.assertEqual(track_1.name, 'guitar-8')
        self.assertEqual(track_2.name, 'guitar-16')
        self.assertEqual(len(track_1.notes), 32)
        self.assertEqual(len(track_2.notes), 48)

        self.assertEqual(len(track_1.bars), 1)
        self.assertEqual(len(track_2.bars), 1)

        self.assertEqual(len(track_1.bars[0].notes), 32)
        self.assertEqual(len(track_2.bars[0].notes), 48)

    def test_3_4_compression(self):
        first_track_name = './test/1/mdup-test-8.mid'
        second_track_name = './test/1/mdup-test-16.mid'
        track_1 = mdup.parse_tracks(first_track_name)[0]
        track_2 = mdup.parse_tracks(second_track_name)[0]
        track_1_compressed = mdup.compress_bar(track_1.bars[0])
        track_2_compressed = mdup.compress_bar(track_2.bars[0])
        self.assertEqual(len(track_1_compressed.notes), 18)
        self.assertEqual(len(track_2_compressed.notes), 18)

    def test_3_4_match_expected_true(self):
        first_track_name = './test/1/mdup-test-8.mid'
        second_track_name = './test/1/mdup-test-16.mid'
        track_1 = mdup.parse_tracks(first_track_name)[0]
        track_2 = mdup.parse_tracks(second_track_name)[0]
        actual = mdup.match_bars(track_1.bars[0], track_2.bars[0])
        self.assertEqual(actual[0], 1)
        self.assertEqual(actual[1], 100)

    def test_3_4_match_2_note_diff(self):
        first_track_name = './test/1/mdup-test-8-2-note-diff.mid'
        second_track_name = './test/1/mdup-test-16.mid'
        track_1 = mdup.parse_tracks(first_track_name)[0]
        track_2 = mdup.parse_tracks(second_track_name)[0]
        actual = mdup.match_bars(track_1.bars[0], track_2.bars[0])
        self.assertEqual(actual[0], 1)
        self.assertEqual(actual[1], 77.8)

    def test_3_4_match_4_note_diff(self):
        first_track_name = './test/1/mdup-test-8-4-note-diff.mid'
        second_track_name = './test/1/mdup-test-16.mid'
        track_1 = mdup.parse_tracks(first_track_name)[0]
        track_2 = mdup.parse_tracks(second_track_name)[0]
        actual = mdup.match_bars(track_1.bars[0], track_2.bars[0])
        self.assertEqual(actual[0], 0)
        self.assertEqual(actual[1], 55.6)


if __name__ == '__main__':
    unittest.main()
