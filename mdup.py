from mido import MidiFile
import logging

logging.basicConfig(level=logging.INFO)

# NOTE:
# diff between notes in different octaves is % 12 == 0
# time_signature:
# numerator - how many beats are in a bar
# denominator - type of beat, it tells which type of note can fit into a semibreve 4 times
# e.g. 4x4 it's four crotchet (quarter note) beats per bar
# e.g. 2x2 has two minim beats per bar


WHOLE = 1
HALF = 2
QUARTER = 4
EIGHTH = 8
SIXTEENTH = 16
THIRTY_SECOND = 32
SIXTY_FOURTH = 64

NOTES_DEFS = [WHOLE, HALF, QUARTER, EIGHTH, SIXTEENTH, THIRTY_SECOND, SIXTY_FOURTH]

WHOLE_NOTE_TIME = 1920

# min number of bars to compare
BARS_LIMIT = 4

time_signature_by_note = dict(map(lambda n: (n, WHOLE_NOTE_TIME / n), NOTES_DEFS))


class Note:
    def __init__(self, val, velocity, time):
        self.val = val
        self.velocity = velocity
        self.time = time

    def get_time(self):
        return self.time


class Chord:
    def __init__(self, notes):
        self.notes = notes

    def get_time(self): next(n for n in self.notes if n.time > 0)


class TimeSignature:
    def __init__(self, numerator, denominator, clocks_per_click, notated_32nd_notes_per_beat, time):
        self.numerator = numerator
        self.denominator = denominator
        self.clocks_per_click = clocks_per_click
        self.notated_32nd_notes_per_beat = notated_32nd_notes_per_beat
        self.time = time


class Bar:
    def __init__(self, numerator, denominator, notes):
        self.numerator = numerator
        self.denominator = denominator
        self.notes = notes

    def notes_values(self):
        return list(map(lambda n: n.val, self.notes))


class Track:
    def __init__(self, index, name, notes, times):
        self.index = index
        self.name = name
        self.notes = notes
        self.times = times
        self.bars = list()

    def create_bars(self):
        def get_notes(notes, start_pos, time_limit):
            end_pos = start_pos
            total_time = 0
            for n in notes[start_pos:]:
                note_time = n.get_time()
                if total_time + note_time <= time_limit:
                    end_pos += 1
                    total_time += note_time
            return end_pos, notes[start_pos:end_pos]

        if len(self.times) == 1 or len([filter(lambda t: t.time > 0, self.times)]) > 0:
            self.bars.append(Bar(self.times[0].numerator, self.times[0].denominator, self.notes))
        else:
            pos = 0
            for index in range(1, len(self.times)):  # default is zero
                time_sig = self.times[index]
                prev_time_sig = self.times[index - 1]
                pruned = get_notes(self.notes, pos, time_sig.time)
                pos = pruned[0]
                self.bars.append(Bar(prev_time_sig.numerator, prev_time_sig.denominator, pruned[1]))
            last = self.times[len(self.times) - 1]
            self.bars.append(Bar(last.numerator, last.denominator, self.notes[pos:]))


def calc_total_time_per_bar(numerator, denominator):
    return BARS_LIMIT * numerator * time_signature_by_note[denominator]


def parse_tracks(path):
    mid = MidiFile(path)
    tracks = []
    for i, t in enumerate(mid.tracks):
        track = Track(i, t.name, [], [])
        logging.info('parse Track %d: %s', i, track.name)
        note_stack = []
        chord = None
        for message in t:  # filter(lambda m: not isinstance(m, midifiles_meta.MetaMessage), track)
            msg_type = message.type
            if msg_type == 'time_signature':
                ts = TimeSignature(message.numerator, message.denominator, message.clocks_per_click,
                                   message.notated_32nd_notes_per_beat, message.time)
                track.times.append(ts)
            if msg_type == 'note_on':
                note_stack.append(message.note)  # skip velocity and time for 'on' message. requires investigation
            if msg_type == 'note_off':
                note_stack.pop()
                note = Note(message.note, message.velocity, message.time)
                if len(note_stack) == 0:
                    if chord is not None:
                        chord.notes.append(note)
                        track.notes.append(chord)
                        chord = None
                    else:
                        track.notes.append(note)
                else:
                    if chord is None:
                        chord = Chord([])
                    chord.notes.append(note)
            logging.debug(message)
        tracks.append(track)
    if len(tracks) == 1:
        return tracks
    tmp_track = tracks[0]
    for track in tracks:
        track.times += tmp_track.times
        track.create_bars()
    return tracks[1:]


# matches two bars and returns percent of their similarity
def match_bars(first_bar, second_bar):
    first_bar_notes = compress_bar(first_bar).notes_values()
    second_bar_notes = compress_bar(second_bar).notes_values()
    first = first_bar_notes
    second = second_bar_notes
    if len(second) < len(first):
        first = second_bar_notes
        second = first_bar_notes

    deletions = count_deletions(first, second)

    match_score = float("%.1f" % (100 - (100 * deletions) / len(first)))
    return (1, match_score) if match_score >= 70 else (0, match_score)


# simply counts amount of deletion ops to get same bars
def count_deletions(first, second):
    deletions = 0
    frequencies = [0] * 1000  # is 1000 enough ?

    for n in first:
        frequencies[n] += 1

    for n in second:
        if frequencies[n] == 0:
            deletions += 1
        else:
            frequencies[n] -= 1

    for f in frequencies:
        if f > 0:
            deletions += f
    return deletions


# simple O(n)
def compress_bar(bar):
    # notes = list(map(lambda n: n.val, bar.notes))
    return Bar(bar.numerator, bar.denominator,
               [n for index, n in enumerate(bar.notes) if
                index == 0 or index > 0 and n.val != bar.notes[index - 1].val])


# matches two notes: returns 1 if both notes are equal, otherwise - 0.
# NOTE: same notes from different octaves considered as equal
def match_notes(first_note, second_note):
    if first_note.val == second_note.val:
        return 1
    diff = first_note.val - second_note.val if first_note.val > second_note.val else second_note.val - first_note.val
    return diff % 12 == 0


# it's tricky one. for simplicity sake let's just compare two first notes of each chord. it should cover most of the cases.
# fixme: does it make sense to support reversed chords ?
def match_chords(first_chord, second_chord):
    m = [first_chord.notes[0:2], second_chord[0:2]]
    return match_notes(m[0][0], m[1][0]) and match_notes(m[0][1], m[1][1])


def print_notes(notes):
    print('size = [{}]'.format(len(notes)), end=' notes: ')
    for n in notes:
        print(n.val, end=", ")
    print('')


def main():
    return 0


if __name__ == '__main__':
    main()
